
#include <stdio.h>
#include <stdlib.h>

#include <time.h>
#include <sys/time.h>

#include <phidget22.h>

// This struct gets passed through the "usrptr" part of the Phidgets API
struct userStruct {
   struct timespec start;
   FILE *outputFile;
};

void CCONV CVoltageChangeHandler(PhidgetVoltageInputHandle IFK, void *userptr, double voltage)
{
    struct timespec now; 
    double timeDifference;  
    clock_gettime(CLOCK_REALTIME, &now);    

    struct userStruct *passedData = (struct userStruct *) userptr;
    FILE *file = passedData->outputFile;

    // Print dot to the screen to indicate one more data point recorded
    printf(".");
    fflush(stdout);

    timeDifference = ((now.tv_sec - passedData->start.tv_sec) + 
        (float) (now.tv_nsec - passedData->start.tv_nsec) / 1000000000.0);
    printf("voltage is %f\n", voltage);
    // 2.018 at 0 steering angle
    printf("steering angle is %f\n", (voltage-1.94)*36);
    fprintf(file, "%6f, %6f \n", timeDifference, voltage);
    fflush(file);
    // return 0;
}

int main(int argc, char* argv[]) {

    int result;
    char character;

    // Obtain an intial time from which to generate timestamps
    struct timespec start;
    clock_gettime(CLOCK_REALTIME, &start);

    // Clear and open the file for writing
    FILE *file = fopen("voltage_data.csv","w");

    struct userStruct userData;
    userData.start = start;
    userData.outputFile = file;

    // Print the header at the top of the file first thing
    fprintf(file, "Time,voltage_data\n");
    
    PhidgetVoltageInputHandle ep;
    PhidgetVoltageInput_create(&ep);
    Phidget_setChannel((PhidgetHandle)ep, 0);
    
    // PhidgetVoltageInput_setDataInterval(ep, 200);
    PhidgetVoltageInput_setOnVoltageChangeHandler(ep, CVoltageChangeHandler, &userData);

    // wait for attachment of Phidget board
    result = Phidget_openWaitForAttachment((PhidgetHandle)ep, 10000);
    if (result!=EPHIDGET_OK) {
        printf("something wrong!\n");
        printf("%d\n", result);
        return 0;
    }
    else
    {
        printf("encoder is OK! \n");
    }

    Phidget_open((PhidgetHandle)ep);

    printf("Press Enter to end\n");
    getchar();

    Phidget_close((PhidgetHandle)ep);
    fclose(file);
    
    return 0;
}